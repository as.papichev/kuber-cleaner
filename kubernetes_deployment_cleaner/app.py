import asyncio

from kubernetes_deployment_cleaner.consts import Consts
from kubernetes_deployment_cleaner.utils.collector import Collector
from kubernetes_deployment_cleaner.utils.validator import Validator


async def gather_data():
    coll = Collector()
    data = await coll.gather_data()
    return data


async def worker_run(data, namespace):
    validator = Validator(data, namespace)
    await validator.validate_deploys()


async def run():
    data = await gather_data()
    _validator_coros = [
        worker_run(data, namespace) for namespace in Consts.WORK_NAMESPACES
    ]
    await asyncio.gather(*_validator_coros)
