import datetime
import asyncio

from RGS_PyLogger.Logger import logger

from kubernetes_deployment_cleaner.consts import Consts
from kubernetes_deployment_cleaner.lib.deployment import Deployment


class DeploymentCleaner:
    @staticmethod
    async def delete_deploy(deployment: Deployment, delete_reason: str, **kwargs):
        """Run subprocess for remove deployment

        Args:
            deployment (Deployment): deployment for delete
            delete_reason (str): message info from check

        Returns:
            result of subprocess
        """
        if not Consts.DEBUG_MODE:
            cmd = f"helm delete --namespace {deployment.namespace} {deployment.release}"
            proc = await asyncio.create_subprocess_shell(
                cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
            )
            stdout, stderr = await proc.communicate()

            if proc.returncode:
                if stderr:
                    logger.warning(
                        f"Can't remove release | {deployment.namespace}:{deployment.release} | ERROR: {stderr.decode()}"
                    )
                if stdout:
                    logger.warning(
                        f"Can't remove release | {deployment.namespace}:{deployment.release} | ERROR: {stdout.decode()}"
                    )
                return False

        logger.success(
            f"Release {deployment.release} was removed from namespace {deployment.namespace} due {delete_reason}"
        )
        await logger.complete()
        return True
