import hvac

class VaultWorker:
    def __init__(self, vault_url, role_id, secret_id):
        self.role_id = role_id
        self.secret_id = secret_id
        self.client = hvac.Client(url=vault_url)
        self.client.auth.approle.login(self.role_id, self.secret_id)

    def get_secret(self, mount_point, path):
        resp = self.client.secrets.kv.v2.read_secret_version(
            mount_point=mount_point, path=path
        )
        return resp["data"]["data"]
