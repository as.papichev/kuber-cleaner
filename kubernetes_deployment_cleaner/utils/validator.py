import re
import asyncio
from typing import Dict

from RGS_PyLogger.Logger import logger

from kubernetes_deployment_cleaner.consts import Consts
from kubernetes_deployment_cleaner.lib.service import Service
from kubernetes_deployment_cleaner.lib.deployment import Deployment
from kubernetes_deployment_cleaner.utils.cleaner import DeploymentCleaner


# TODO: разбить на таски и хранить отдельно
class Validator:
    def __init__(
        self, data: Dict, namespace: str, deploy_term_time: int = Consts.TERM_DAYS
    ):
        self.data = data
        self.datetime = Consts.TODAY
        self.deploy_term_time = deploy_term_time
        self.namespace = namespace

    @property
    def namespaced_data(self):
        """Gather data for namespace"""
        return self.data.get(self.namespace)

    @staticmethod
    def check_service_is_defroute(service: Service) -> bool:
        """Try get Service.def_route attribute"""
        if not service:
            return False

        if service.def_route:
            return True
        return False

    async def __delete_deploy(self, deploy: Deployment, delete_reason: str, **kwargs):
        result = await DeploymentCleaner.delete_deploy(deploy, delete_reason, **kwargs)
        return bool(result)

    # Positive checks
    async def __check_no_clean(self, deploy: Deployment):
        """Checks deploy have noClean attribute"""
        return bool(deploy.no_clean)

    async def __check_release_is_latest(self, deploy: Deployment):
        """Checks deploy from master ref, or have -latest suffix"""
        return bool((
            re.search(Consts.SKIP_SUFFIXES, deploy.release)
            or deploy.ref_name == "master"
        ))

    async def __check_switched_on_defroute(self, deploy: Deployment):
        """Checks deployment switched on defRoute"""
        defRoute = deploy.def_route
        serviceDefRoute = self.namespaced_data.get("def_routes").get(defRoute)

        try:
            if (
                serviceDefRoute.def_route
                and deploy.app == serviceDefRoute.selector_app
                and deploy.release == serviceDefRoute.selector_release
            ):
                return True
        except AttributeError as e:
            if re.match(r"'NoneType' object has no attribute 'def_route'", *e.args):
                logger.debug(e)
                logger.warning(
                    f"release {deploy.namespace}:{deploy.release} has no attribute deploy.def_route"
                )
        return False

    async def __check_prod_env(self, deploy: Deployment):
        """Checks deploy environment

        Returns:
            if env == production: True
            else: True
        """
        return (
            deploy.environment == "production"
            or deploy.temp_environment == "production"
        )

    # Negative checks

    async def __check_status(self, deploy: Deployment):
        """Checks deploy available"""
        if not deploy.available_replicas:
            delete_reason = "release not available"
            await self.__delete_deploy(deploy=deploy, delete_reason=delete_reason)
            return True
        return False

    async def __check_deploy_date(
        self, deploy: Deployment, ttl: int = Consts.TERM_DAYS
    ):
        """Checks deploy.updated_date

        Args:
            deploy (Deployment): deployment for validation
            ttl (int, optional): deployment livetime. Defaults to self.deploy_term_time.

        Returns:
            if today - updated_date >= termination_time (default 6): True
            else: False
        """
        delete_reason = "too old release"

        if not deploy.updated_date:
            delete_reason = "too old chart"
            await self.__delete_deploy(deploy=deploy, delete_reason=delete_reason)
            return True

        deployAge = self.datetime - deploy.updated_date

        if deployAge.days >= ttl:
            await self.__delete_deploy(deploy=deploy, delete_reason=delete_reason)
            return True
        return False

    # Checks run
    async def __positive_checks(self, deploy: Deployment):
        """Run "Positive" checks

        Args:
            deploy (Deployment): deployment for validation

        Returns:
            coros returns [bool * len(coros)]

            if True in coros[bool]: True
            else: False
        """
        coros = [
            self.__check_prod_env(deploy),
            self.__check_no_clean(deploy),
            self.__check_release_is_latest(deploy),
            self.__check_switched_on_defroute(deploy),
        ]
        result = await asyncio.gather(*coros)
        return True in result

    async def __negative_checks(self, deploy: Deployment):
        """Run "Negative" checks

        Args:
            deploy (Deployment): deployment for validation

        Returns:
            coros returns [bool * len(coros)]

            if True in coros[bool]: True
            else: False
        """
        coros = [
            self.__check_status(deploy),
            self.__check_deploy_date(deploy),
        ]
        result = await asyncio.gather(*coros)
        return

    async def __validate_deploy(self, deploy: Deployment):
        """Run checks for deployment

        Args:
            deploy (Deployment): deployment for validation
        """
        if await self.__positive_checks(deploy):
            return

        await self.__negative_checks(deploy)
        return

    async def validate_deploys(self):
        """Run coroutines for each deployment in namespace"""
        coros = [
            self.__validate_deploy(deploy)
            for deploy in self.namespaced_data.get("deployments").values()
        ]
        await asyncio.gather(*coros)
        return
