import asyncio
from typing import Dict, List
from collections import defaultdict

from kubernetes_deployment_cleaner.consts import Consts
from kubernetes_deployment_cleaner.client.kuber_worker import KuberWorker
from kubernetes_deployment_cleaner.utils.validator import Validator
from kubernetes_deployment_cleaner.lib.service import Service
from kubernetes_deployment_cleaner.lib.deployment import Deployment


class Collector:
    def __init__(self, namespaces: List[str] = Consts.WORK_NAMESPACES):
        self.namespaces = namespaces
        self.kuber_worker = KuberWorker(Consts.K8S_CONTEXT)
        self.data = defaultdict()

    async def __collect_deployments(self, namespace: str) -> List[Deployment]:
        """Collects deployments from namespace"""
        coros = []

        deployments = self.kuber_worker.k8s_appsclient.list_namespaced_deployment(
            namespace
        ).items
        coros = [
            self.kuber_worker.create_deployment_object(deploy) for deploy in deployments
        ]
        deploymentsList = await asyncio.gather(*coros)
        return deploymentsList

    async def __collect_services(self, namespace: str) -> List[Service]:
        """Collects services from all namespaces"""
        coros = []

        services = self.kuber_worker.k8s_client.list_namespaced_service(namespace).items
        coros = [
            self.kuber_worker.create_service_object(service) for service in services
        ]
        servicesList = await asyncio.gather(*coros)
        return servicesList

    async def __collect_def_routes(self, services):
        """Collect defRoutes from service list"""
        defRouteList = filter(Validator.check_service_is_defroute, services)
        return defRouteList

    async def __fill_data(self, namespace, defroutes, services, deployments):
        """Writing collected data to self.data: defaultdict
        self.data format:
        data = {
            namespace: {
                def_routes: {name: Service},
                services: {name: Service},
                deployments: {name: Deployment},
            }
        }
        """
        self.data[namespace] = {}
        self.data[namespace]["def_routes"] = {route.name: route for route in defroutes}
        self.data[namespace]["services"] = {
            service.name: service for service in services if service
        }
        self.data[namespace]["deployments"] = {
            deployment.name: deployment for deployment in deployments
        }
        return

    async def __collect_all_namespace_items(self, namespace: str):
        """Collect all items (services, deployments, defRoutes) of namespace
        After collecting write to self.data
        """
        services = await self.__collect_services(namespace)
        deployments = await self.__collect_deployments(namespace)
        defRoutes = await self.__collect_def_routes(services)
        await self.__fill_data(namespace, defRoutes, services, deployments)
        return

    async def gather_data(self):
        coros = [
            self.__collect_all_namespace_items(namespace)
            for namespace in self.namespaces
        ]
        await asyncio.gather(*coros)
        return self.data
