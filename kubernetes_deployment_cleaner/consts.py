import datetime
import os
from typing import List

from RGS_PyLogger.Logger import logger


# Global vars
ENVIRONMENT = os.getenv("CLEANER_ENVIRONMENT")
DEBUG_MODE = os.getenv("DEBUG_MODE", "")

VAULT_URL = os.getenv("VAULT_URL")
VAULT_ROLE_ID = os.getenv("VAULT_ROLE_ID")
VAULT_SECRET_ID = os.getenv("VAULT_SECRET_ID")

KUBERNETES_CONFIG_PATH = os.getenv("KUBERNETES_CONFIG_PATH", "/app/config/kubeconf")
TERM_DAYS = 2
DEFAULT_NAMESPACES = [
    "omnychannel",
    "marketplace",
]
CONTEXTS = {"DEV": "rgsdev", "PROD": "rgsprod"}


class ConstSetter:
    @staticmethod
    def set_namespaces(env: str, ns_list: List[str]):
        
        newNsList = [*ns_list]
        if env == "DEV":
            for ns in ns_list:
                newNsList.append(f"test-{ns}")
        if env == "PROD":
            for ns in ns_list:
                newNsList.append(f"stg-{ns}")
        return newNsList

    @staticmethod
    def set_context(env):
        context = CONTEXTS.get(env, "unknown")
        if context == "unknown":
            logger.critical(f"Can't get context for {ENVIRONMENT}")
        return context


class Consts:
    DEBUG_MODE = DEBUG_MODE
    TERM_DAYS = TERM_DAYS
    VAULT_ROLE_ID = VAULT_ROLE_ID
    VAULT_SECRET_ID = VAULT_SECRET_ID
    TODAY = datetime.date.today()
    SKIP_SUFFIXES = r".*-(develop|def|latest)$"
    K8S_CONTEXT = ConstSetter.set_context(ENVIRONMENT)
    KUBERNETES_CONFIG_PATH = KUBERNETES_CONFIG_PATH
    WORK_NAMESPACES = ConstSetter.set_namespaces(ENVIRONMENT, DEFAULT_NAMESPACES)
