from typing import Dict


class Service:
    def __init__(self, service_raw_obj: Dict):
        self.name = service_raw_obj.get("name")
        self.namespace = service_raw_obj.get("namespace")
        self.def_route = service_raw_obj.get("defRoute")
        self.def_route_to = service_raw_obj.get("defRouteTo")
        self.selector_app = service_raw_obj.get("selector_app")
        self.selector_release = service_raw_obj.get("selector_release")
