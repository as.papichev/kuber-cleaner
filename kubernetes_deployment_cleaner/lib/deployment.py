from typing import Dict
import dateutil.parser

from RGS_PyLogger.Logger import logger


class Deployment:
    """Deployment object"""

    def __init__(self, deployment_raw_obj: Dict):
        self.app = deployment_raw_obj.get("app")
        self.release = deployment_raw_obj.get("release")
        self.project_id = deployment_raw_obj.get("projectId")
        self.job_id = deployment_raw_obj.get("jobId")
        self.ref_name = deployment_raw_obj.get("refName")
        self.raw_updated_date = deployment_raw_obj.get("deployUpdatedDate")
        self.def_route = deployment_raw_obj.get("defRoute")
        self.no_clean = deployment_raw_obj.get("noClean")
        self.name = deployment_raw_obj.get("name")
        self.namespace = deployment_raw_obj.get("namespace")
        # TODO: delete when all deploys refresh for new chart version ( >= 0.1.1 )
        self.temp_environment = deployment_raw_obj.get("temp_environment")  # DEPRECATED
        self.environment = deployment_raw_obj.get("environment")
        self.available_replicas = deployment_raw_obj.get("available_replicas")
        self.creation_timestamp = deployment_raw_obj.get("creation_timestamp")

    @property
    def updated_date(self):
        date = None
        try:
            date = dateutil.parser.parse(self.raw_updated_date).date()
        except Exception as e:
            logger.debug(e)
            logger.warning(
                f"Can't create attribute self.updated_date release |\n{self.namespace}:{self.release} | self.raw_updated_date = {self.raw_updated_date}"
            )
        return date
