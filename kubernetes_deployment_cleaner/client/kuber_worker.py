import base64
from typing import Dict

from kubernetes import client, config
from RGS_PyLogger.Logger import logger

from kubernetes_deployment_cleaner.consts import Consts
from kubernetes_deployment_cleaner.utils.client.vault.worker import VaultWorker
from kubernetes_deployment_cleaner.lib.deployment import Deployment
from kubernetes_deployment_cleaner.lib.service import Service


class KuberWorker:
    """Kubernetes client and item wrapper"""

    def __init__(self, k8s_context):
        self.k8s_context = k8s_context

    @property
    def k8s_client(self):
        """Init Kubernetes CoreV1Api client"""
        vw = VaultWorker(vault_url=Consts.VAULT_URL, role_id=Consts.VAULT_ROLE_ID, secret_id=Consts.VAULT_SECRET_ID)
        kube_config = vw.get_secret("devcloud", "gitlab").get("KUBECONF")

        with open(Consts.KUBERNETES_CONFIG_PATH, "w") as f:
            f.write(base64.b64decode(kube_config).decode("utf-8"))

        config.load_kube_config(
            config_file=Consts.KUBERNETES_CONFIG_PATH, context=self.k8s_context
        )
        return client.CoreV1Api()

    @property
    def k8s_appsclient(self):
        """Init Kubernetes AppsV1Api client"""
        config.load_kube_config(
            config_file=Consts.KUBERNETES_CONFIG_PATH, context=self.k8s_context
        )
        return client.AppsV1Api()

    # DEPRECATED
    # TODO: delete when all deploys refresh for new chart version ( >= 0.1.1 )
    async def oldstyle_k8s_env(self, envs):
        try:
            env = [env for env in envs if env.name == "K8S_ENVIRONMENT"]
            env = env[0].value
        except Exception as e:
            logger.debug(e)
            env = ""
        return env

    async def __generate_raw_deployment_object(self, item: Dict) -> Dict:
        """Generate raw item for Deployment builder class"""
        raw_deploy_object = {
            "app": item.metadata.labels.get("app"),
            "release": item.metadata.labels.get("release"),
            "deployUpdatedDate": item.metadata.labels.get("deployUpdatedDate"),
            "defRoute": item.metadata.labels.get("defService"),
            "projectId": item.metadata.labels.get("projectId"),
            "jobId": item.metadata.labels.get("jobId"),
            "refName": item.metadata.labels.get("refName"),
            "noClean": item.metadata.labels.get("noClean"),
            "name": item.metadata.name,
            "namespace": item.metadata.namespace,
            "temp_environment": await self.oldstyle_k8s_env(
                item.spec.template.spec.containers[0].env
            ),
            "environment": item.metadata.labels.get("environment"),
            "available_replicas": item.status.available_replicas,
            "creation_timestamp": item.metadata.creation_timestamp,
        }
        return raw_deploy_object

    async def __generate_raw_service_object(self, item: Dict) -> Dict:
        """Generate raw item for Service builder class"""
        raw_service_object = {
            "defRoute": item.metadata.labels.get("default_route"),
            "defRouteTo": item.metadata.labels.get("def_route_to"),
            "selector_app": item.spec.selector.get("app"),
            "selector_release": item.spec.selector.get("release"),
            "name": item.metadata.name,
            "namespace": item.metadata.namespace,
            "creation_timestamp": item.metadata.creation_timestamp,
        }
        return raw_service_object

    async def __wrap_deploy_item(self, raw_obj: Dict) -> Deployment:
        """Wraps raw object into Deployment"""
        try:
            deployment = Deployment(raw_obj)
        except:
            print("Can't create Deployment from item", raw_obj.get("name"))

        return deployment

    async def __wrap_service_item(self, raw_obj: Dict) -> Service:
        """Wraps raw object into Service"""
        try:
            service = Service(raw_obj)
        except:
            print("Can't create Service from item", raw_obj.metadata.name)

        return service

    async def create_deployment_object(self, item: Dict) -> Deployment:
        raw_obj = await self.__generate_raw_deployment_object(item)
        deployment = await self.__wrap_deploy_item(raw_obj)
        return deployment

    async def create_service_object(self, item: Dict) -> Service:
        try:
            raw_obj = await self.__generate_raw_service_object(item)
            service = await self.__wrap_service_item(raw_obj)
        except Exception as e:
            logger.debug(e)
            logger.warning(f"Can't create service object from\n{item}")
            return
        return service
