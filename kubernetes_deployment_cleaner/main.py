from datetime import datetime

BEGIN = datetime.now()


import asyncio

from RGS_PyLogger.Logger import logger
from kubernetes_deployment_cleaner.app import run


def start_cleaning():
    asyncio.run(run())

    # Finish
    END = datetime.now()
    result_time = END - BEGIN

    logger.success(f"All done!")
    logger.success(f"Work completed in {result_time}")
