from setuptools import find_packages, setup
import os

setup(
    name="rgs-k8s-cleaner",
    description="CI tools",
    version=os.getenv("CI_COMMIT_REF_NAME"),
    url=os.getenv("CI_JOB_URL"),
    author=os.getenv("GITLAB_USER_NAME"),
    author_email=os.getenv("GITLAB_USER_EMAIL"),
    license="MIT",
    packages=find_packages(
        include=["kubernetes_deployment_cleaner", "kubernetes_deployment_cleaner.*"]
    ),
    install_requires=[
        "kubernetes==12.0.1",
        "click==7.1.2",
        "hvac==0.10.8",
        "rgs_pylogger==1.0.0",
    ],
    keywords=["", ""],
    entry_points={
        "console_scripts": [
            "rgs-k8s-cleaner=kubernetes_deployment_cleaner.main:start_cleaning"
        ],
    },
)
